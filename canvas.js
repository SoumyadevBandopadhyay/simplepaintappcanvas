var canvas= document.getElementById("canvas")
var clear= document.getElementById("clearButton")
canvas.width= window.innerWidth
canvas.height= window.innerHeight

var c= canvas.getContext('2d')


function Triangle(x0,y0,x1,y1,x2,y2,r, g, b){
    this.x0=x0
    this.x1=x1
    this.x2=x2
    this.y0=y0
    this.y1=y1
    this.y2=y2
    this.r=r
    this.g=g
    this.b=b
    
    this.draw= function(){
        c.fillStyle=`rgba(${r},${g},${b})`
        c.beginPath()
        c.moveTo(x0,y0)
        c.lineTo(x1,y1)
        c.lineTo(x2,y2)
        c.fill()
    }
}

var x0=0,x1=0,x2=0,y0=0,y1=0,y2=0
var triArr=[]
var isDragging={
    bool: false,
}
function startPosition(e){
    x0=e.clientX
    y0=e.clientY

    triArr.map((tri,i,arr)=>{
        if(x0>=tri.x1 && x0<=tri.x2 && ((y0>=tri.y0 && y0<=tri.y2)||(y0<=tri.y0 && y0>=tri.y2))){
            isDragging.bool=true
            isDragging.initX= x0
            isDragging.initY= y0
        }
    })
}

function finishedPosition(e){
    x1=x0-(Math.abs(y0-e.clientY))
    x2=x0+(Math.abs(y0-e.clientY))
    y1=e.clientY
    y2=e.clientY
    var obj={
        x0:x0,
        x1:x1,
        x2:x2,
        y0:y0,
        y1:y1,
        y2:y2
    }
    if(x0!=x1 && !isDragging.bool){
        triArr.push(new Triangle(obj.x0, obj.y0, obj.x1, obj.y1, obj.x2, obj.y2, Math.random()*255, Math.random()*255, Math.random()*255))
    }else if(isDragging.bool && x0!=e.clientX){
        triArr.map((tri,i,arr)=>{
            if(isDragging.initX>=tri.x1 && isDragging.initX<=tri.x2){
                obj.x1=e.clientX-(Math.abs(tri.x0-tri.x1))
                obj.x2=e.clientX+(Math.abs(tri.x0-tri.x2))
                obj.x0=e.clientX
                obj.y1=e.clientY+(Math.abs(tri.y0-tri.y1))
                obj.y2=e.clientY+(Math.abs(tri.y0-tri.y2))
                obj.y0=e.clientY
                obj.r=tri.r
                obj.g=tri.g
                obj.b=tri.b
                arr.splice(i,1)
            }
            return tri
        })
        triArr.push(new Triangle(obj.x0, obj.y0, obj.x1, obj.y1, obj.x2, obj.y2, obj.r, obj.g, obj.b))
        isDragging.bool=false
        c.clearRect(0, 0, window.innerWidth, window.innerHeight)
    }
    triArr.map(tri=>{
        tri.draw() 
    })
}

function deleteTriangle(e){
    let x=e.clientX
    let y=e.clientY
    triArr.map((tri,i,arr)=>{
        if(x>=tri.x1 && x<=tri.x2 && ((y>=tri.y0 && y<=tri.y2)||(y<=tri.y0 && y>=tri.y2))){
            arr.splice(i,1)
            c.clearRect(0, 0, window.innerWidth, window.innerHeight)
        }else{
            return tri
        }
    })
    triArr.map(tri=>{
        tri.draw()
    })
}

function clearCanvas(){
    c.clearRect(0,0, window.innerWidth, window.innerHeight)
    triArr=[]
}

function animate(){
    requestAnimationFrame(animate)
    canvas.addEventListener('mousedown', startPosition)
    canvas.addEventListener('mouseup', finishedPosition)
    canvas.addEventListener('dblclick', deleteTriangle)
    clear.addEventListener('click', clearCanvas)

    
}
animate()